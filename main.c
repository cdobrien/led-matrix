#include "graphics.h"
#include "ledMatrix.h"
#include "ledMatrixDriver.h"
#include "maze.h"
#include "random.h"

#define MAZE_START_X 1
#define MAZE_START_Y 1

#define MAZE_FINISH_X (MAZE_WIDTH - 2)
#define MAZE_FINISH_Y (MAZE_HEIGHT - 2)

#define SEED 314159265

int
main()
{
   Coordinate_t start, finish;
   Colour_t pixelBuffer[MATRIX_WIDTH][MATRIX_HEIGHT];
   Maze_Cell_t maze[MAZE_WIDTH][MAZE_HEIGHT];
   uint16_t i;

   LedMatrixDriver_Init();
   Random_Init(SEED);

   Graphics_Fill(pixelBuffer, Colour_Black);
   start.x = MAZE_START_X;
   start.y = MAZE_START_Y;
   finish.x = MAZE_FINISH_X;
   finish.y = MAZE_FINISH_Y;

   while (1) {
      Maze_Generate(maze, pixelBuffer);
    
      for (i = 0; i < 128; i++)
         LedMatrixDriver_Draw(pixelBuffer);

      Maze_Solve(maze, pixelBuffer, start, finish);

      for (i = 0; i < 128; i++)
         LedMatrixDriver_Draw(pixelBuffer);

   }
}

