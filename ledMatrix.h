#ifndef LED_MATRIX
#define LED_MATRIX

#include "stm32f4xx.h"
#include "system_stm32f4xx.h"
#include "stm32f4_discovery.h"

#define MATRIX_WIDTH  32
#define MATRIX_HEIGHT 32

typedef enum {
   Colour_Black = 0b000,
   Colour_Blue = 0b001,
   Colour_Green = 0b010,
   Colour_Cyan = 0b011,
   Colour_Red = 0b100,
   Colour_Magenta = 0b101,
   Colour_Yellow = 0b110,
   Colour_White = 0b111
} Colour_t;

typedef struct {
   uint8_t x;
   uint8_t y;
} Coordinate_t;

#endif /* LED_MATRIX */

