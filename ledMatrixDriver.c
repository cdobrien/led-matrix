#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"

#include "ledMatrix.h"
#include "ledMatrixDriver.h"

#define CONTROL_PORT     GPIOD
#define CONTROL_CLOCK    RCC_AHB1Periph_GPIOD
#define ADDRESS_A        GPIO_Pin_0
#define ADDRESS_B        GPIO_Pin_1
#define ADDRESS_C        GPIO_Pin_2
#define ADDRESS_D        GPIO_Pin_3
#define CLOCK            GPIO_Pin_4
#define LATCH            GPIO_Pin_5
#define OUTPUT_ENABLE    GPIO_Pin_6
#define ALL_CONTROL_PINS (ADDRESS_A | ADDRESS_B | ADDRESS_C | ADDRESS_D | CLOCK | LATCH | OUTPUT_ENABLE)

#define DATA_PORT        GPIOA
#define DATA_CLOCK       RCC_AHB1Periph_GPIOA
#define UPPER_RED        GPIO_Pin_0
#define UPPER_GREEN      GPIO_Pin_1
#define UPPER_BLUE       GPIO_Pin_2
#define LOWER_RED        GPIO_Pin_3
#define LOWER_GREEN      GPIO_Pin_4
#define LOWER_BLUE       GPIO_Pin_5
#define ALL_DATA_PINS    (UPPER_RED | UPPER_GREEN | UPPER_BLUE | LOWER_RED | LOWER_GREEN | LOWER_BLUE)

#define LATCH_WAIT 1024

void writeAddress(
   uint8_t sectionIndex
);

void writeColour(
   uint16_t redPin,
   uint16_t greenPin,
   uint16_t bluePin,
   Colour_t colour
);

void
LedMatrixDriver_Init()
{
   GPIO_InitTypeDef GpioInitOptions;

   GpioInitOptions.GPIO_Mode  = GPIO_Mode_OUT;
   GpioInitOptions.GPIO_Speed = GPIO_Speed_2MHz;
   GpioInitOptions.GPIO_OType = GPIO_OType_PP;
   GpioInitOptions.GPIO_PuPd = GPIO_PuPd_NOPULL;

   RCC_AHB1PeriphClockCmd(CONTROL_CLOCK, ENABLE);
   GpioInitOptions.GPIO_Pin = ALL_CONTROL_PINS;
   GPIO_Init(CONTROL_PORT, &GpioInitOptions);
   GPIO_ResetBits(CONTROL_PORT, CLOCK);
   GPIO_ResetBits(CONTROL_PORT, LATCH);
   GPIO_SetBits(CONTROL_PORT, OUTPUT_ENABLE);
   GPIO_ResetBits(CONTROL_PORT, ADDRESS_A);
   GPIO_ResetBits(CONTROL_PORT, ADDRESS_B);
   GPIO_ResetBits(CONTROL_PORT, ADDRESS_C);
   GPIO_ResetBits(CONTROL_PORT, ADDRESS_D);

   RCC_AHB1PeriphClockCmd(DATA_CLOCK, ENABLE);
   GpioInitOptions.GPIO_Pin = ALL_DATA_PINS;
   GPIO_Init(DATA_PORT, &GpioInitOptions);
   GPIO_ResetBits(DATA_PORT, UPPER_RED);
   GPIO_ResetBits(DATA_PORT, UPPER_GREEN);
   GPIO_ResetBits(DATA_PORT, UPPER_BLUE);
   GPIO_ResetBits(DATA_PORT, LOWER_RED);
   GPIO_ResetBits(DATA_PORT, LOWER_GREEN);
   GPIO_ResetBits(DATA_PORT, LOWER_BLUE);
}

void
LedMatrixDriver_Draw(Colour_t pixelBuffer[MATRIX_WIDTH][MATRIX_HEIGHT])
{ 
   uint16_t sectionIndex, i;
   Colour_t upperColour, lowerColour;

   for (sectionIndex = 0; sectionIndex < ((MATRIX_HEIGHT + 1) / 2); sectionIndex++) {
      writeAddress(sectionIndex);

      GPIO_SetBits(CONTROL_PORT, OUTPUT_ENABLE);
      GPIO_ResetBits(CONTROL_PORT, LATCH);

      for (i = 0; i < (MATRIX_WIDTH); i++) {
         GPIO_ResetBits(CONTROL_PORT, CLOCK);

         upperColour = pixelBuffer[i][sectionIndex];
         lowerColour = pixelBuffer[i][sectionIndex + ((MATRIX_HEIGHT + 1) / 2)];

         writeColour(UPPER_RED, UPPER_GREEN, UPPER_BLUE, upperColour);
         writeColour(LOWER_RED, LOWER_GREEN, LOWER_BLUE, lowerColour);

         GPIO_SetBits(CONTROL_PORT, CLOCK);
      }

      GPIO_ResetBits(CONTROL_PORT, CLOCK);

      GPIO_SetBits(CONTROL_PORT, LATCH);

      GPIO_ResetBits(CONTROL_PORT, OUTPUT_ENABLE);

      for (i = 0; i < LATCH_WAIT; i++)
         ;
      GPIO_ResetBits(CONTROL_PORT, LATCH);
   }
}

void
writeAddress(uint8_t sectionIndex)
{
   uint8_t addressA, addressB, addressC, addressD;

   addressA = sectionIndex & 1;
   addressB = (sectionIndex >> 1) & 1;
   addressC = (sectionIndex >> 2) & 1;
   addressD = (sectionIndex >> 3) & 1;

   if (addressA)
      GPIO_SetBits(CONTROL_PORT, ADDRESS_A);
   else
      GPIO_ResetBits(CONTROL_PORT, ADDRESS_A);

   if (addressB)
      GPIO_SetBits(CONTROL_PORT, ADDRESS_B);
   else
      GPIO_ResetBits(CONTROL_PORT, ADDRESS_B);

   if (addressC)
      GPIO_SetBits(CONTROL_PORT, ADDRESS_C);
   else
      GPIO_ResetBits(CONTROL_PORT, ADDRESS_C);

   if (addressD)
      GPIO_SetBits(CONTROL_PORT, ADDRESS_D);
   else
      GPIO_ResetBits(CONTROL_PORT, ADDRESS_D);
}

void
writeColour(uint16_t redPin, uint16_t greenPin, uint16_t bluePin, Colour_t colour)
{
   uint8_t red, green, blue;

   red = ((uint8_t)colour >> 2) & 1;
   green = ((uint8_t)colour >> 1) & 1;
   blue = (uint8_t)colour & 1;

   if (red)
      GPIO_SetBits(DATA_PORT, redPin);
   else
      GPIO_ResetBits(DATA_PORT, redPin);

   if (green)
      GPIO_SetBits(DATA_PORT, greenPin);
   else
      GPIO_ResetBits(DATA_PORT, greenPin);

   if (blue)
      GPIO_SetBits(DATA_PORT, bluePin);
   else
      GPIO_ResetBits(DATA_PORT, bluePin);
}

