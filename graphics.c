#include "ledMatrix.h"
#include "graphics.h"

void
Graphics_SetPixel(Colour_t pixelBuffer[MATRIX_WIDTH][MATRIX_HEIGHT], Colour_t colour, Coordinate_t location)
{
   pixelBuffer[location.x][location.y] = colour;
}

void
Graphics_Fill(Colour_t pixelBuffer[MATRIX_WIDTH][MATRIX_HEIGHT], Colour_t colour)
{
   uint8_t x, y;

   for (x = 0; x < MATRIX_WIDTH; x++)
      for (y = 0; y < MATRIX_WIDTH; y++)
         pixelBuffer[x][y] = colour;
}

void
Graphics_Rectangle(Colour_t pixelBuffer[MATRIX_WIDTH][MATRIX_HEIGHT], Colour_t colour, Coordinate_t topLeft, uint8_t width, uint8_t height)
{
    Coordinate_t topRight, bottomRight, bottomLeft;

    topRight = bottomRight = bottomLeft = topLeft;
    topRight.x += width;
    bottomRight.x += width;
    bottomRight.y += height;
    bottomLeft.y += height;

    Graphics_LineSegment(pixelBuffer, colour, topLeft, topRight);
    Graphics_LineSegment(pixelBuffer, colour, topRight, bottomRight);
    Graphics_LineSegment(pixelBuffer, colour, bottomRight, bottomLeft);
    Graphics_LineSegment(pixelBuffer, colour, bottomLeft, topLeft);
}

void
Graphics_FilledRectangle(Colour_t pixelBuffer[MATRIX_WIDTH][MATRIX_HEIGHT], Colour_t colour, Coordinate_t topLeft, uint8_t width, uint8_t height)
{
   uint8_t i, j;

   for (i = 0; i < width; i++)
      for (j = 0; j < height; j++)
         pixelBuffer[topLeft.x + i][topLeft.y + j] = colour;
}

void
Graphics_LineSegment(Colour_t pixelBuffer[MATRIX_WIDTH][MATRIX_HEIGHT], Colour_t colour, Coordinate_t from, Coordinate_t to)
{
   int8_t rise, run, delta, i;

   rise = to.y - from.y;
   run = to.x - from.x;

   if (run != 0) {
      delta = (run > 0) ? 1 : (-1);

      for (i = 0; i != run; i += delta)
         pixelBuffer[from.x + i][from.y + ((i * rise) / run)] = colour;

      pixelBuffer[from.x + i][from.y + ((i * rise) / run)] = colour;
   } else {
      delta = (rise > 0) ? 1 : (-1);

      for (i = 0; i != rise; i += delta)
         pixelBuffer[from.x][from.y + i] = colour;

      pixelBuffer[from.x][from.y + i] = colour;
   }
}

