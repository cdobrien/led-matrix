include config.mk

vpath %.c $(STM_SRC)

SRCS    = main.c ledMatrixDriver.c graphics.c maze.c random.c system_stm32f4xx.c stm32f4xx_rcc.c stm32f4xx_gpio.c \
          $(STM_DIR)/Libraries/CMSIS/ST/STM32F4xx/Source/Templates/TrueSTUDIO/startup_stm32f4xx.s
HEADERS = ledMatrix.h ledMatrixDriver.h graphics.h maze.h random.h

.PHONY: all options $(PROJ_NAME) clean dist flash

all: options $(PROJ_NAME)

options:
	@echo :: Build options for $(PROJ_NAME)-$(VERSION) :::::::::::::::::::::::::::::::::::
	@echo "STM_DIR     = $(STM_DIR)"
	@echo
	@echo "CC          = $(CC)"
	@echo "OBJCOPY     = $(OBJCOPY)"
	@echo "ST_FLASH    = $(ST_FLASH)"
	@echo
	@echo "CFLAGS      = $(CFLAGS)"
	@echo "LFLAGS      = $(LFLAGS)"
	@echo

$(PROJ_NAME): $(PROJ_NAME).elf $(PROJ_NAME).hex $(PROJ_NAME).bin

$(PROJ_NAME).elf: $(SRCS)
	$(CC) $(CFLAGS) $(LFLAGS) $^ -o $@ 

$(PROJ_NAME).hex: $(PROJ_NAME).elf
	$(OBJCOPY) -O ihex $(PROJ_NAME).elf   $(PROJ_NAME).hex

$(PROJ_NAME).bin: $(PROJ_NAME).elf
	$(OBJCOPY) -O binary $(PROJ_NAME).elf $(PROJ_NAME).bin

clean:
	rm -f *.o $(PROJ_NAME).elf $(PROJ_NAME).hex $(PROJ_NAME).bin $(PROJ_NAME)-*.txz

dist: clean
	mkdir $(PROJ_NAME)-$(VERSION)
	cp -R Makefile config.mk stm32_flash.ld *.c *.h $(PROJ_NAME)-$(VERSION)
	tar czf $(PROJ_NAME)-$(VERSION).txz $(PROJ_NAME)-$(VERSION)
	rm -rf $(PROJ_NAME)-$(VERSION)

flash: $(PROJ_NAME).bin
	$(ST_FLASH) write $(PROJ_NAME).bin 0x8000000

