#include "random.h"

static uint32_t Random_state;

void
Random_Init(uint32_t seed)
{
   Random_state = seed;
}

uint32_t
Random_GetRandomNumber()
{
   Random_state ^= Random_state << 13;
   Random_state ^= Random_state >> 17;
   Random_state ^= Random_state << 5;

   return Random_state;
}

