#ifndef MAZE
#define MAZE

#include "ledMatrix.h"

#define MAZE_WIDTH  31
#define MAZE_HEIGHT 31

typedef enum {
   Cell_Wall,
   Cell_Unvisited,
   Cell_Visited
} Maze_Cell_t;

void Maze_Generate(
   Maze_Cell_t maze[MAZE_WIDTH][MAZE_HEIGHT],
   Colour_t pixelBuffer[MATRIX_WIDTH][MATRIX_HEIGHT]
);

void Maze_Solve(
   Maze_Cell_t maze[MAZE_WIDTH][MAZE_HEIGHT],
   Colour_t pixelBuffer[MATRIX_WIDTH][MATRIX_HEIGHT],
   Coordinate_t start,
   Coordinate_t finish
);

#endif /* MAZE */

