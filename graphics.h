#ifndef GRAPHICS
#define GRAPHICS

#include "ledMatrix.h"

void Graphics_SetPixel(
   Colour_t pixelBuffer[MATRIX_WIDTH][MATRIX_HEIGHT],
   Colour_t colour,
   Coordinate_t location
);

void Graphics_Fill(
   Colour_t pixelBuffer[MATRIX_WIDTH][MATRIX_HEIGHT],
   Colour_t colour
);

void Graphics_Rectangle(
   Colour_t pixelBuffer[MATRIX_WIDTH][MATRIX_HEIGHT],
   Colour_t colour,
   Coordinate_t topLeft,
   uint8_t width,
   uint8_t height
);

void Graphics_FilledRectangle(
   Colour_t pixelBuffer[MATRIX_WIDTH][MATRIX_HEIGHT],
   Colour_t colour,
   Coordinate_t topLeft,
   uint8_t width,
   uint8_t height
);

void Graphics_LineSegment(
   Colour_t pixelBuffer[MATRIX_WIDTH][MATRIX_HEIGHT],
   Colour_t colour,
   Coordinate_t from,
   Coordinate_t to
);

#endif /* GRAPHICS */

