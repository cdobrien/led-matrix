#ifndef RANDOM
#define RANDOM

#include "ledMatrix.h"

void Random_Init(
   uint32_t seed
);

uint32_t Random_GetRandomNumber(
   void
);

#endif /* RANDOM */

