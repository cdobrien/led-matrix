#ifndef LED_MATRIX_DRIVER
#define LED_MATRIX_DRIVER 

#include "ledMatrix.h"

void LedMatrixDriver_Init(
   void
);

void LedMatrixDriver_Draw(
   Colour_t pixelBuffer[MATRIX_WIDTH][MATRIX_HEIGHT]
);

#endif /* LED_MATRIX_DRIVER */

