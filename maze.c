#include "stm32f4xx_rcc.h"
#include "stm32f4xx_rng.h"

#include "random.h"
#include "ledMatrixDriver.h"
#include "maze.h" 

#define GENERATION_START_X (MAZE_WIDTH - 2)
#define GENERATION_START_Y (MAZE_HEIGHT - 2)

#define WALL_COLOUR      Colour_White
#define UNVISITED_COLOUR Colour_Black
#define VISITED_COLOUR   Colour_Blue
#define LOCATION_COLOUR  Colour_Red

#define MAX_NEIGHBORS 4

#define GENERATION_DRAW_WAIT 1
#define SOLVING_DRAW_WAIT    4

uint8_t Maze_getUnvisitedNeighbors(
   Maze_Cell_t maze[MAZE_WIDTH][MAZE_HEIGHT],
   Coordinate_t neighbors[MAX_NEIGHBORS],
   Coordinate_t location
);

void Maze_draw(
   Maze_Cell_t maze[MAZE_WIDTH][MAZE_HEIGHT],
   Colour_t pixelBuffer[MATRIX_WIDTH][MATRIX_HEIGHT],
   Coordinate_t location,
   Colour_t wallColour,
   Colour_t unvisitedColour,
   Colour_t visitedColour,
   Colour_t locationColour,
   uint8_t wait
);

uint8_t Maze_getAccessableUnvisitedNeighbors(
   Maze_Cell_t maze[MAZE_WIDTH][MAZE_HEIGHT],
   Coordinate_t neighbors[MAX_NEIGHBORS],
   Coordinate_t location
);

void
Maze_Generate(Maze_Cell_t maze[MAZE_WIDTH][MAZE_HEIGHT], Colour_t pixelBuffer[MATRIX_WIDTH][MATRIX_HEIGHT])
{
   Coordinate_t stack[MAZE_WIDTH * MAZE_HEIGHT], unvisitedNeighbors[MAX_NEIGHBORS], location;
   uint16_t x, y, stackPosition, unvisitedNeighborCount;

   for (x = 0; x < MAZE_WIDTH; x++) {
      for (y = 0; y < MAZE_HEIGHT; y++) {
         if (x % 2 == 1 && y % 2 == 1)
            maze[x][y] = Cell_Unvisited;
         else
            maze[x][y] = Cell_Wall;
      }
   }

   location.x = GENERATION_START_X;
   location.y = GENERATION_START_Y;
   maze[location.x][location.y] = Cell_Visited;

   stackPosition = 0;
    while (1) {
      Maze_draw(maze, pixelBuffer, location, WALL_COLOUR, WALL_COLOUR, UNVISITED_COLOUR, UNVISITED_COLOUR, GENERATION_DRAW_WAIT);

      unvisitedNeighborCount = Maze_getUnvisitedNeighbors(maze, unvisitedNeighbors, location);
      
      if (unvisitedNeighborCount != 0) {
         Coordinate_t nextLocation, wallLocation;
         uint8_t rand;

         rand = Random_GetRandomNumber() % unvisitedNeighborCount;
         nextLocation = unvisitedNeighbors[rand];

         wallLocation.x = (location.x + nextLocation.x) / 2;
         wallLocation.y = (location.y + nextLocation.y) / 2;
         maze[wallLocation.x][wallLocation.y] = Cell_Visited;

         stack[stackPosition++] = location;

         location = nextLocation;
         maze[location.x][location.y] = Cell_Visited;
      } else {
         if (stackPosition == 0)
            break;

         location = stack[--stackPosition];
      }
   }

   Maze_draw(maze, pixelBuffer, location, WALL_COLOUR, WALL_COLOUR, UNVISITED_COLOUR, UNVISITED_COLOUR, GENERATION_DRAW_WAIT);

   for (x = 0; x < MAZE_WIDTH; x++)
      for (y = 0; y < MAZE_HEIGHT; y++)
         if (maze[x][y] == Cell_Visited)
            maze[x][y] = Cell_Unvisited;
}

void
Maze_Solve(Maze_Cell_t maze[MAZE_WIDTH][MAZE_HEIGHT], Colour_t pixelBuffer[MATRIX_WIDTH][MATRIX_HEIGHT], Coordinate_t start, Coordinate_t finish)
{
   Coordinate_t stack[MAZE_WIDTH * MAZE_HEIGHT], neighbors[MAX_NEIGHBORS], location;
   uint16_t stackPosition, neighborCount;

   stackPosition = 0;
   location = start;
   maze[location.x][location.y] = Cell_Visited;

    while (location.x != finish.x || location.y != finish.y) {
      Maze_draw(maze, pixelBuffer, location, WALL_COLOUR, UNVISITED_COLOUR, VISITED_COLOUR, LOCATION_COLOUR, SOLVING_DRAW_WAIT);

      neighborCount = Maze_getAccessableUnvisitedNeighbors(maze, neighbors, location);

      if (neighborCount != 0) {
         Coordinate_t pathLocation, nextLocation;
         uint16_t rand;

         rand = Random_GetRandomNumber() % neighborCount;
         nextLocation = neighbors[rand];

         pathLocation.x = (location.x + nextLocation.x) / 2;
         pathLocation.y = (location.y + nextLocation.y) / 2;
         maze[pathLocation.x][pathLocation.y] = Cell_Visited;

         stack[stackPosition++] = location;

         location = nextLocation;
         maze[location.x][location.y] = Cell_Visited;
      } else {
         if (stackPosition == 0)
            break;

         location = stack[--stackPosition];
      }
   }

   Maze_draw(maze, pixelBuffer, location, WALL_COLOUR, UNVISITED_COLOUR, VISITED_COLOUR, LOCATION_COLOUR, SOLVING_DRAW_WAIT);
}

uint8_t
Maze_getUnvisitedNeighbors(Maze_Cell_t maze[MAZE_WIDTH][MAZE_HEIGHT], Coordinate_t unvisitedNeighbors[MAX_NEIGHBORS], Coordinate_t location)
{
   Coordinate_t neighbor;
   uint8_t unvisitedNeighborCount = 0;

   if (location.x > 2) {
      neighbor.x = location.x - 2;
      neighbor.y = location.y;
      if (maze[neighbor.x][neighbor.y] == Cell_Unvisited)
         unvisitedNeighbors[unvisitedNeighborCount++] = neighbor;
   }

   if (location.x < (MAZE_WIDTH - 3)) {
      neighbor.x = location.x + 2;
      neighbor.y = location.y;
      if (maze[neighbor.x][neighbor.y] == Cell_Unvisited)
         unvisitedNeighbors[unvisitedNeighborCount++] = neighbor;
   }

   if (location.y > 2) {
      neighbor.x = location.x;
      neighbor.y = location.y - 2;
      if (maze[neighbor.x][neighbor.y] == Cell_Unvisited)
         unvisitedNeighbors[unvisitedNeighborCount++] = neighbor;
   }

   if (location.y < (MAZE_WIDTH - 3)) {
      neighbor.x = location.x;
      neighbor.y = location.y + 2;
      if (maze[neighbor.x][neighbor.y] == Cell_Unvisited)
         unvisitedNeighbors[unvisitedNeighborCount++] = neighbor;
   }

   return unvisitedNeighborCount;
}

void
Maze_draw(Maze_Cell_t maze[MAZE_WIDTH][MAZE_HEIGHT], Colour_t pixelBuffer[MATRIX_WIDTH][MATRIX_HEIGHT], Coordinate_t location, Colour_t wallColour, Colour_t unvisitedColour, Colour_t visitedColour, Colour_t locationColour, uint8_t wait)
{
   uint8_t x, y;

   for (x = 0; x < MAZE_WIDTH; x++) {
      for (y = 0; y < MAZE_HEIGHT; y++) {
         if (x == location.x && y == location.y) {
            pixelBuffer[x][y] = locationColour;
            continue;
         }

         switch (maze[x][y]) {
         case Cell_Wall:
            pixelBuffer[x][y] = wallColour;
            break;
         case Cell_Unvisited:
            pixelBuffer[x][y] = unvisitedColour;
            break;
         case Cell_Visited:
            pixelBuffer[x][y] = visitedColour;
            break;
         }
      }
   }

   for (x = 0; x < wait; x++)
      LedMatrixDriver_Draw(pixelBuffer);
}

uint8_t
Maze_getAccessableUnvisitedNeighbors(Maze_Cell_t maze[MAZE_WIDTH][MAZE_HEIGHT], Coordinate_t neighbors[MAX_NEIGHBORS], Coordinate_t location)
{
   Coordinate_t neighbor, path;
   uint8_t count;

   count = 0;

   if (location.x > 2) {
      neighbor.x = location.x - 2;
      neighbor.y = location.y;
      path.x = location.x - 1;
      path.y = location.y;
      if (maze[path.x][path.y] != Cell_Wall && maze[neighbor.x][neighbor.y] == Cell_Unvisited)
         neighbors[count++] = neighbor;
   }

   if (location.x < (MAZE_WIDTH - 3)) {
      neighbor.x = location.x + 2;
      neighbor.y = location.y;
      path.x = location.x + 1;
      path.y = location.y;
      if (maze[path.x][path.y] != Cell_Wall && maze[neighbor.x][neighbor.y] == Cell_Unvisited)
         neighbors[count++] = neighbor;
   }

   if (location.y > 2) {
      neighbor.x = location.x;
      neighbor.y = location.y - 2;
      path.x = location.x;
      path.y = location.y - 1;
      if (maze[path.x][path.y] != Cell_Wall && maze[neighbor.x][neighbor.y] == Cell_Unvisited)
         neighbors[count++] = neighbor;
   }

   if (location.y < (MAZE_WIDTH - 3)) {
      neighbor.x = location.x;
      neighbor.y = location.y + 2;
      path.x = location.x;
      path.y = location.y + 1;
      if (maze[path.x][path.y] != Cell_Wall && maze[neighbor.x][neighbor.y] == Cell_Unvisited)
         neighbors[count++] = neighbor;
   }

   return count;
}

